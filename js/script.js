// Теоретичні питання

// 1. Опишіть, як можна створити новий HTML тег на сторінці.
//    Використовуємо метод createElement('elem'), let newTag = document.createElement('tag');
// 2. Опишіть, що означає перший параметр функції insertAdjacentHTML і опишіть можливі варіанти цього параметра.
//    У функції insertAdjacentHTML перший параметр означає куди буде переміщений елемент з другого параметра.
//    Варіанти параметра beforebegin, afterbegin, beforeend, afterend.
// 3. Як можна видалити елемент зі сторінки?
//    Видалити елемент зі сторінки можна за допомогою метода remove().
//
//     Завдання
//     Реалізувати функцію, яка отримуватиме масив елементів і виводити їх на сторінку у вигляді списку. Завдання має бути
// виконане на чистому Javascript без використання бібліотек типу jQuery або React.
//
//     Технічні вимоги:
//     Створити функцію, яка прийматиме на вхід масив і опціональний другий аргумент parent - DOM-елемент, до якого буде
// прикріплений список (по дефолту має бути document.body.
//     кожен із елементів масиву вивести на сторінку у вигляді пункту списку;
// Приклади масивів, які можна виводити на екран:
//
//     ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];
// ["1", "2", "3", "sea", "user", 23];
// Можна взяти будь-який інший масив.
//     Необов'язкове завдання підвищеної складності:
// 1.Додайте обробку вкладених масивів. Якщо всередині масиву одним із елементів буде ще один масив, виводити його як
// вкладений список. Приклад такого масиву:
//
//     ["Kharkiv", "Kiev", ["Borispol", "Irpin"], "Odessa", "Lviv", "Dnieper"];
// Підказка: використовуйте map для обходу масиву та рекурсію, щоб обробити вкладені масиви.
// 2.Очистити сторінку через 3 секунди. Показувати таймер зворотного відліку (лише секунди) перед очищенням сторінки.

let array = ['London', ['Arsenal', 'West Ham', 'Tottenham', 'Chelsea', 'Fulham', 'QPR'], 'Madrid', ['Atletico', 'Real', 'Rayo']];

function testList(array,parent = document.body) {
    let arrList = newList(array).join('');
    let arrUl = document.createElement('ul');
    function newList(array) {
        return array.map(point => {
            if (Array.isArray(point)) {
                return point = `<li><ul>${newList(point).join('')}</ul></li>`;
            } else {
                return point = `<li>${point}</li>`;
            }
        })
    };
    arrUl.innerHTML = arrList
    parent.prepend(arrUl);
};

let counter = 3;
let div = document.createElement('div');
div.innerText = `It will end in ${counter}`;
document.body.prepend(div);

let timer = setInterval(() => {
    counter--;
    div.innerText = `It will end in ${counter}`;
}, 1000);
setTimeout(() => {
    clearInterval(timer);
    document.body.innerHTML = '';
}, 3000);
testList(array);